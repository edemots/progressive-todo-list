import { update, remove } from '../utils/interact';

export default function Item(todo) {
  const todoTemplate = `
  <article class="bg-gray-200 p-4 mb-2 ${todo.done ? 'completed' : ''}">
    <a href="/todo/${todo.id}">
      <div class="flex items-center">
        <div class="flex-1">
          <input id="${todo.id}" type="checkbox" ${todo.done ? 'checked' : ''} class="form-checkbox border-gray-400 h-4 w-4 text-indigo-600" />
          <label class="ml-3 font-medium text-gray-700 cursor-pointer">${todo.title}</label>
        </div>
        <svg class="text-red-500 fill-current w-6 h-6 js-remove-item cursor-pointer" viewBox="0 0 20 20">
          <path d="M9 2a1 1 0 00-.894.553L7.382 4H4a1 1 0 000 2v10a2 2 0 002 2h8a2 2 0 002-2V6a1 1 0 100-2h-3.382l-.724-1.447A1 1 0 0011 2H9zM7 8a1 1 0 012 0v6a1 1 0 11-2 0V8zm5-1a1 1 0 00-1 1v6a1 1 0 102 0V8a1 1 0 00-1-1z" clip-rule="evenodd" fill-rule="evenodd"></path>
        </svg>
      </div>
    </a>
  </article>
  `;

  let todoElement = document.createElement('article');
  todoElement.innerHTML = todoTemplate;
  todoElement = todoElement.firstElementChild;

  todoElement.querySelector('.js-remove-item').addEventListener('click', () => {
    if (confirm('Supprimer le todo ?')) {
      todo.removed = true;
      remove(todo);
      todoElement.remove();
    }
  });

  let checkbox = todoElement.querySelector('input');

  todoElement.querySelector('.form-checkbox').addEventListener('click', () => {
    if (todoElement.classList.contains('completed')) {
      todo.done = false;
      checkbox.removeAttribute('checked');
      todoElement.classList.remove('completed');
    } else {
      todo.done = true;
      checkbox.setAttribute('checked', 'checked');
      todoElement.classList.add('completed');
    }
    update(todo);
  });

  return todoElement;
}
