import { save } from '../utils/interact';
import Item from '../components/item';

export default function Index(app, todos) {
  const pageTemplate = `
    <div class="h-full flex flex-col">
      <h1 class="text-4xl font-bold text-center mt-4 mb-6">What Todo</h1>
      <section class="flex-1" id="todo-list-container"> </section>
      <section class="p-4 flex flex-col border-t border-gray-400">
        <div class="flex flex-wrap -mx-3 mb-6">
          <div class="w-full px-3">
            <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-first-name">
              Titre
            </label>
            <input type="text" class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="todo-name">
          </div>
        </div>
        <div class="flex flex-wrap -mx-3 mb-6">
          <div class="w-full px-3">
            <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-first-name">
              Description
            </label>
            <input type="text" class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="todo-description">
          </div>
        </div>
        <button class="bg-blue-500 hover:bg-blue-300 text-white font-bold py-2 px-4 rounded inline-flex items-center self-end" id="add-item">
          <svg class="fill-current w-4 h-4 mr-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
            <path d="M13.586 3.586a2 2 0 112.828 2.828l-.793.793-2.828-2.828.793-.793zM11.379 5.793L3 14.172V17h2.828l8.38-8.379-2.83-2.828z"></path>
          </svg>
          <span>Créer le Todo</span>
        </button>
      </section>
    </div>
  `;

  let pageElement = document.createElement('div');
  pageElement.innerHTML = pageTemplate;
  pageElement = pageElement.firstElementChild;

  app.appendChild(pageElement);

  let todoListContainer = pageElement.querySelector('#todo-list-container');

  todos.forEach(todo => {
    let todoElement = Item(todo);
    todoListContainer.appendChild(todoElement);
  });

  let button = pageElement.querySelector('#add-item');
  button.addEventListener('click', () => {

    let nameInput = pageElement.querySelector('#todo-name');
    let nameValue = nameInput.value;

    let descInput = pageElement.querySelector('#todo-description');
    let descValue = descInput.value;

    if (nameValue.length <= 0 && descValue.length <= 0) {
      return;
    }

    let todo = {
      id: Date.now(),
      title: nameValue,
      desc: descValue,
      synced: true,
      updated: false,
      done: false,
      removed: false,
      date: Date.now(),
    };

    let todoItemElement = Item(todo);
    todoListContainer.appendChild(todoItemElement);

    save(todo);

    nameInput.value = '';
    descInput.value = '';
  });
}
