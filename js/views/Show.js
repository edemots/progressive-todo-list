
export default function Show(app, todo) {
  const pageTemplate = `
  <h1>${todo.title}</h1>
  `;

  let pageElement = document.createElement('div');
  pageElement.innerHTML = pageTemplate;
  pageElement = pageElement.firstElementChild;

  app.appendChild(pageElement);
}
