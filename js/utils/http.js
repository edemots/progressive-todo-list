const http = {
  get(url) {
    return request(url, 'GET');
  },
  post(url, body) {
    return request(url, 'POST', body);
  },
  patch(url, body) {
    return request(url, 'PATCH', body);
  },
  delete(url) {
    return request(url, 'DELETE');
  },
};

function request(url, method, body = null) {
  const options = {
    method: method,
    headers: {
      'Content-Type': 'application/json',
    },
  };
  if (body) options.body = JSON.stringify(body);
  return fetch(url, options);
}

export default http;
