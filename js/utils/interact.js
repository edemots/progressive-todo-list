import { deleteTodo, patchTodo, postTodo } from '../api/todo';
import { deleteLocalTodo, saveLocalTodo } from '../idb';

export const save = async todo => {
  if (!document.offline) {
    const result = await postTodo(todo);
    if (result !== false) {
      saveLocalTodo(todo);
      return;
    }
  }
  dispatchError(todo);
};

export const update = async todo => {
  if (!document.offline) {
    const result = await patchTodo(todo);
    if (result !== false) {
      saveLocalTodo(todo);
      return;
    }
  }

  dispatchError(todo);
};

export const remove = async todo => {
  if (!document.offline) {
    const result = await deleteTodo(todo);
    if (result !== false) {
      deleteLocalTodo(todo);
      return;
    }
  }
  dispatchError(todo);
};

const dispatchError = async todo => {
  console.error('[Error] Network Error');
  todo.synced = false;
  saveLocalTodo(todo);
};
