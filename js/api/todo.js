import http from '../utils/http';

let api = 'http://localhost:3000';

export async function indexTodo() {
  return http.get(`${api}/todos`).
    then(result => result.json()).
    catch(error => {
      console.error(error);
      return false;
    });
}

export async function showTodo(id) {
  return http.get(`${api}/todos/${id}`).
    then(result => result.json()).
    catch(error => {
      console.error(error);
      return false;
    });
}

export async function postTodo(data) {
  return http.post(`${api}/todos`, data).
    then(result => result.json()).
    catch(error => {
      console.error(error);
      return false;
    });
}

export async function patchTodo(data) {
  return http.patch(`${api}/todos/${data.id}`, data).
    then(result => result.json()).
    catch(error => {
      console.error(error);
      return false;
    });
}

export async function deleteTodo(id) {
  return http.delete(`${api}/todos/${id}`).
    then(result => result.json()).
    catch(error => {
      console.error(error);
      return false;
    });
}
