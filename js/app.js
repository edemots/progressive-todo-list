'use strict';

import page from 'page';
import checkConnectivity from './network.js';

import { indexTodo, showTodo } from './api/todo.js';
import {
  saveLocalTodos,
  saveLocalTodo,
  getLocalTodos,
  getLocalTodo,
  getTodoToCreate,
  getTodoToUpdate,
  getTodoToDelete,
} from './idb.js';
import { remove, save, update } from './utils/interact.js';

checkConnectivity({ interval: 2000 });

const synchronize = async () => {
  let todos = await getTodoToCreate();
  for (let todo of todos || []) {
    save(todo);
  }

  todos = await getTodoToUpdate();
  for (let todo of todos || []) {
    update(todo);
  }

  todos = await getTodoToDelete();
  for (let todo of todos || []) {
    remove(todo);
  }
};

document.addEventListener('connection-changed', e => {
  document.offline = !e.detail;
  if (!document.offline) {
    synchronize();
  }
});

const app = document.querySelector('#app');

page('/', async () => {
  const module = await import('/js/views/Index.js');
  const Index = module.default;

  let todos = [];
  if (!document.offline) {
    const data = await indexTodo();
    todos = await saveLocalTodos(data);
  } else {
    todos = await getLocalTodos() || [];
  }

  await synchronize();

  app.innerHTML = '';
  Index(app, todos);
});

page('/todos/:id', async (ctx) => {
  const module = await import('/js/views/Show.js');
  const Show = module.default;

  const base = document.head.querySelector('base');
  document.title = `${base.dataset.base} - `;

  const todoId = ctx.params.id;
  let todo = {};
  if (!document.offline) {
    const data = await showTodo(todoId);
    todo = await saveLocalTodo(data);
  } else {
    todo = await getLocalTodo(todoId) || [];
  }

  app.innerHTML = '';
  Show(app, todo);
});

page();
